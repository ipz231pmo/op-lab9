﻿#include <stdio.h>
#include <Windows.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRun = 1;
    do {
        printf("\nОбчислення добутку та суми послідовності чисел. Введіть послідовність чисел. Для завершення введіть нуль.\n");
        double sum = 0, prod = 1, number;
        do {
            scanf_s("%lf", &number);
            if (number) prod *= number;
            sum += number;
        } while (number);
        printf("Сумма чисел %f\n", sum);
        printf("Добуток чисел %f\n", prod);
        printf("1 - продовжити роботу, 2 – завершити роботу.\n");
        scanf_s("%d", &isRun);
    } while(isRun != 2);
}