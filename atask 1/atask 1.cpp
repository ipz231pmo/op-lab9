﻿#include <stdio.h>
#include <Windows.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRun;
    do {
        printf("Введіть 10 чисел\n");
        int big5 = 0, big10 = 0;
        double number;
        for (int i = 0; i < 10; i++) {
            scanf_s("%lf", &number);
            if (number > 10) big10++;
            if (number > 5) big5++;
        }
        if (big10 > 4) 
            printf("Караул!\n");
        else 
            printf("Більше 5 %d чисел, більше 10 %d чиисел\n", big5, big10);
        printf("\n1 - продовжити роботу, 2 – завершити роботу.\n");
        scanf_s("%d", &isRun);
    } while (isRun != 2);
}