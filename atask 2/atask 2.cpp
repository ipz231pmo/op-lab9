﻿#include <stdio.h>
#include <Windows.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRun = 1;
    do {
        printf("Введіть затримку перед поданням звукового сигналу у с\n");
        int num;
        scanf_s("%d", &num);
        num *= 1000;
        Sleep(num);
        printf("\a");

        printf("\n1 - продовжити роботу, 2 – завершити роботу.\n");
        scanf_s("%d", &isRun);
    } while (isRun != 2);
}