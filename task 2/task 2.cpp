﻿#include <stdio.h>
#include <Windows.h>
int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    int isRun;
    do {
        printf("Обчислення добутку та суми 2-ї та 3-ї цифр 4 цифрового числа. Введіть чотирицифрове число\n");
        int number;
        scanf_s("%d", &number);
        int n2 = (number / 100) % 10;
        int n3 = (number / 10) % 10;
        printf("Добуток 2 та 3 цифр: %d\n", n2*n3);
        printf("Сумма 2 та 3 цифр: %d\n", n2+n3);
        printf("\n1 - продовжити роботу, 2 – завершити роботу.\n");
        scanf_s("%d", &isRun);
    } while(isRun !=2);
}